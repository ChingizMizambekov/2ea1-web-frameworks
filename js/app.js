var app = angular.module('labTutorApp', []);

app.controller('RoomController', function ($scope) {
    $scope.room = {
        numberOfQuestions: 7
    };

    var defaultExerciseStatus = function () {
        var tempArr = [];
        for (var i = 0; i < $scope.room.numberOfQuestions; i++) {
            tempArr.push({
                completed: false
            });
        }
        return tempArr;
    };

    $scope.room.currentUser = {
        name: '',
        noExercisesCompleted: defaultExerciseStatus()
    };

    $scope.room.otherUsers = [
        {
            name: 'Tenesha Thorn',
            noExercisesCompleted: defaultExerciseStatus()
    },
        {
            name: 'Isaiah Imperato',
            noExercisesCompleted: defaultExerciseStatus()
    },
        {
            name: 'Tesha Town',
            noExercisesCompleted: defaultExerciseStatus()
    },
        {
            name: 'Lynetta Lemus',
            noExercisesCompleted: defaultExerciseStatus()
    },
        {
            name: 'Christeen Currence',
            noExercisesCompleted: defaultExerciseStatus()
    },
        {
            name: 'Tamatha Thrasher',
            noExercisesCompleted: defaultExerciseStatus()
    },
        {
            name: 'Irvin Iliff',
            noExercisesCompleted: defaultExerciseStatus()
    },
        {
            name: 'Stephanie Steveson',
            noExercisesCompleted: defaultExerciseStatus()
    },
        {
            name: 'Janee Jong',
            noExercisesCompleted: defaultExerciseStatus()
    },
        {
            name: 'Kellye Kirtley',
            noExercisesCompleted: defaultExerciseStatus()
    },
        {
            name: 'Rebecca Reddin',
            noExercisesCompleted: defaultExerciseStatus()
    },
        {
            name: 'Tamela Tien',
            noExercisesCompleted: defaultExerciseStatus()
    },
        {
            name: 'Leopoldo Lucarelli',
            noExercisesCompleted: defaultExerciseStatus()
    },
        {
            name: 'Molly Marcello',
            noExercisesCompleted: defaultExerciseStatus()
    },
        {
            name: 'Wendi Wray',
            noExercisesCompleted: defaultExerciseStatus()
    },
        {
            name: 'Bernard Borders',
            noExercisesCompleted: defaultExerciseStatus()
    }
  ];

    $scope.allExercisesCompleted = function (user) {
        var exerciseCompleted = true;
        user.noExercisesCompleted.forEach(function (exercise) {
            if (!exercise.completed) {
                exerciseCompleted = false;
            }
        });
        return exerciseCompleted;
    };



});
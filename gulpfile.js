var gulp = require("gulp"),
    browserSync = require("browser-sync").create();

// Static server + watching css/html/js files
gulp.task("serve", function() {
    browserSync.init({
        server: {
            baseDir: "./"
        }
    });
    
    gulp.watch("css/*.css", ["styleInject"]);
    gulp.watch("js/*.js").on("change", browserSync.reload);
    gulp.watch("*.html").on("change", browserSync.reload);
    
});


// Inject CSS into browser
gulp.task("styleInject", function() {
    return gulp.src("css/*.css")
    .pipe(browserSync.stream());
});

// Inject JS into
gulp.task("jsInject", function() {
    return gulp.src("js/*.js")
    .pipe(browserSync.stream());
});